# README #

This README describes the different SQL Statements used within our Database to extract the necessary data.

### What is this repository for? ###

Starting from the ETL-Prozess to distribute the imported data from EDEKA until the visualisation of the data via dynamic views, this repository includes all relevant SQL-Statements

### How do I get set up? ###

To be able to use these statements, you need access to our database. You find a guidebook here: https://cyreen.atlassian.net/wiki/spaces/CYR/pages/80150529

### Who do I talk to? ###

Main responsible for this repository is Davide 